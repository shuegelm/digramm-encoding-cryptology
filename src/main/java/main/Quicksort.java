package main;

import javafx.util.Pair;

import java.util.ArrayList;

/**
 * Created by liqSTAR on 27.12.2017.
 */
public class Quicksort {

    public static ArrayList<Pair<String,Double>> quickSort(ArrayList<Pair<String,Double>> pairArrayList, int p, int r)
    {
        if(p<r)
        {
            int q=partition(pairArrayList,p,r);
            quickSort(pairArrayList,p,q);
            quickSort(pairArrayList,q+1,r);
        }

        return pairArrayList;
    }

    private static int partition(ArrayList<Pair<String, Double>> pairArrayList, int p, int r) {

        double x = pairArrayList.get(p).getValue();
        int i = p-1 ;
        int j = r+1 ;

        while (true)
        {
            i++;
            while ( i< r && pairArrayList.get(i).getValue().compareTo(x) > 0)
                i++;
            j--;
            while (j>p && pairArrayList.get(j).getValue().compareTo(x) < 0)
                j--;

            if (i < j)
                swap(pairArrayList, i, j);
            else
                return j;
        }
    }

    private static void swap(ArrayList<Pair<String, Double>> pairArrayList, int i, int j) {
        Pair<String, Double> temp = pairArrayList.get(i);
        pairArrayList.set(i, pairArrayList.get(j));
        pairArrayList.set(j, temp);
    }

}
