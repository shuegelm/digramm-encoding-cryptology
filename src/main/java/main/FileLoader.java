package main;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by liqSTAR on 26.12.2017.
 */
public class FileLoader {
    public static String[][] keyTextFileLoader(String fileName) {

        String[][] keysTable = new String[26][26];

        try {
            FileReader fileReader = new FileReader(fileName);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            String line = bufferedReader.readLine();
            String[] array;
            int zeilen = 0;

            while (line!=null) {
                line = removeWhitespaces(line);
                if (!line.equals("")) {
                    array = line.split(" ");
                    for (int i = 0; i < array.length; i++) {
                        if (!array[i].equals("")) {
                            try {
                                keysTable[zeilen][i] = array[i];
                            }
                            catch (ArrayIndexOutOfBoundsException e) {
                                System.out.println("Scheiß Array wieder mal zu klein. In Zeile: " + zeilen +  " Message: " +e);
                            }
                        }
                    }
                    zeilen++;
                } else {
                }
                line = bufferedReader.readLine();

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    return keysTable;
    }

    public static ArrayList<String[]> PlainTextFileLoader(String fileName) {

        //oder in einen String laden?
        ArrayList<String[]> plainTextArrayList =new ArrayList<String[]>();
        try {
            FileReader fileReader = new FileReader(fileName);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            String line = bufferedReader.readLine();
            String[] array;

            while (line!=null) {
                array = line.split(" ");
                plainTextArrayList.add(array);
                line = bufferedReader.readLine();
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    return plainTextArrayList;
    }
    public static String plainTextFileLoaderAsString(String fileName) {

        String plainText = "";
        try {
            FileReader fileReader = new FileReader(fileName);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            String line = bufferedReader.readLine();
            String[] array;
            StringBuilder sb = new StringBuilder();

            while (line!=null) {
                array = line.split(" ");
                for (String s : array) {
                    sb.append(s);
                }
                line = bufferedReader.readLine();
            }

            plainText = sb.toString();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return plainText;
    }

    public static String removeWhitespaces(String s) {
        Pattern pattern = Pattern.compile("\\s+");
        Matcher matcher = pattern.matcher(s);
        String noWhitespace = matcher.replaceAll(" ");
        return noWhitespace;
    }

}
