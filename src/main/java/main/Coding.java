package main;

import javafx.util.Pair;

import java.util.ArrayList;

/**
 * Created by liqSTAR on 27.12.2017.
 */
public class Coding
{

    private static String[][] keyTable = new String[26][26];
    private static String[] abc = {"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"};
    private static String[] mostGermanDigramms = {"en","er","ch","de","te","nd","ei","ie","in","es"};
    //private static String[] mostGermanDigramms = {"EN","ER","CH","DE","TE","ND","EI","IE","IN","ES"};

    public static String encoding(String plainText, String[][] key) {

        keyTable = key;
        StringBuilder cipherText = new StringBuilder();

        int zeile = 0, spalte = 0;

        for (int i = 0; i < plainText.length(); i++) {
            zeile = getLetterNumber(plainText.charAt(i));
            if (i+1 >= plainText.length()) {
                break;
            }
            spalte = getLetterNumber(plainText.charAt(i+1));
            try {
                cipherText.append(keyTable[zeile][spalte]);

            }
            catch (ArrayIndexOutOfBoundsException e) {
                System.out.println("Beim Verschlüsseln ist was kaputt: " + e + " mit Zeile: " + zeile + " und Spalte " + spalte);
            }
            i++;
        }



        return cipherText.toString();
    }

    public static String decodingWithKey(String cipherText, String[][] key) {

        keyTable = key;
        StringBuilder plainText = new StringBuilder();

        String digramm = "";
        String digrammPlain = "";

        for (int i = 0; i < cipherText.length(); i++) {
            digramm = "" + cipherText.charAt(i) + cipherText.charAt(i+1);
            digrammPlain = getPlainDigramm(digramm);
            plainText.append(digrammPlain);
            i++;
        }
        return plainText.toString();
    }

    public static ArrayList<Pair<String,Double>> countDigramms(String cipherText) {
        double anzahlDigramms = 0;
        int[][] digrammTable = new int[26][26];
        ArrayList<Pair<String,Double>> digrammArrayList = new ArrayList<Pair<String, Double>>();
        int zeile = 0;
        int spalte = 0;

        for (int i = 0; i < 26; i++) {
            for (int j = 0; j < 26; j++) {
                digrammTable[i][j]=0;
            }
        }

        for (int i = 0; i < cipherText.length(); i++) {
            zeile = getLetterNumber(cipherText.charAt(i));
            spalte = getLetterNumber(cipherText.charAt(i+1));
            digrammTable[zeile][spalte]++;
            i++;
        }

        for (int i = 0; i < 26; i++) {
            for (int j = 0; j < 26; j++) {
                if (digrammTable[i][j]!=0) {
                    anzahlDigramms++;
                }
            }
        }

        for (int i = 0; i < 26; i++) {
            for (int j = 0; j < 26; j++) {
                if (digrammTable[i][j]!=0) {
                    digrammArrayList.add(new Pair<String,Double>(""+abc[i]+abc[j], ((double)digrammTable[i][j] * 100 / anzahlDigramms)));
                }
            }
        }

        return digrammArrayList;
    }

    public static ArrayList<String> decideAndReplace(ArrayList<Pair<String, Double>> digrammsArrayList, String cipherText, String givenWord) {

        String[][] keyTable = new String[26][26];
        String plainText = "";
        int encodingDigrammFirstLetter = 0,encodingDigrammSecondLetter=0,mostGermanDigrammFirstLetter=0,mostGermanDigrammSecondLetter=0, startKeyTableFirstLetter =0,startKeyTableSecondLetter=0;
        ArrayList<String> possiblePlainTextsArrayList = new ArrayList<String>();
        ArrayList<Integer> possibleWordPositions;

        for (int s = 0; s < 10; s++) {
            //r mit 17
            encodingDigrammFirstLetter = getLetterNumber(digrammsArrayList.get(s).getKey().charAt(0));
            //o mit 14
            encodingDigrammSecondLetter = getLetterNumber(digrammsArrayList.get(s).getKey().charAt(1));

            //e mit 4
            mostGermanDigrammFirstLetter = getLetterNumber(mostGermanDigramms[s].charAt(0));
            //n mit 13
            mostGermanDigrammSecondLetter = getLetterNumber(mostGermanDigramms[s].charAt(1));

            //r - n = e
            startKeyTableFirstLetter = (encodingDigrammFirstLetter - mostGermanDigrammSecondLetter);
            if (startKeyTableFirstLetter < 0) startKeyTableFirstLetter+=26;
            //o - e = k
            startKeyTableSecondLetter = (encodingDigrammSecondLetter - mostGermanDigrammFirstLetter);
            if (startKeyTableSecondLetter < 0) startKeyTableSecondLetter+=26;

            for (int i = 0; i < 26; i++) {
                for (int j = 0; j < 26; j++) {
                    keyTable[j][i] = abc[startKeyTableFirstLetter]+abc[startKeyTableSecondLetter];
                    startKeyTableSecondLetter++;
                    if (startKeyTableSecondLetter == 26) {
                        startKeyTableSecondLetter = 0;
                    }
                }
                startKeyTableSecondLetter = (encodingDigrammSecondLetter - mostGermanDigrammFirstLetter);
                if (startKeyTableSecondLetter < 0) startKeyTableSecondLetter+=26;
                startKeyTableFirstLetter++;
                if (startKeyTableFirstLetter == 26) {
                    startKeyTableFirstLetter = 0;
                }
            }
            plainText += decodingWithKey(cipherText,keyTable);
            possibleWordPositions = searchPositions(plainText,givenWord);

            if (!possibleWordPositions.isEmpty()) {
                plainText = ("\nMostGerman Digramm \"" + mostGermanDigramms[s] + "\" Replacing:" + "\n" + (s+1) + ". plaintext is: \n") + printBeautiful(plainText) + "\nPossible word positions for \"" + givenWord + "\": ";
                for (int i = 0;i < possibleWordPositions.size();i++) {
                    plainText= plainText + possibleWordPositions.get(i);
                }
                possiblePlainTextsArrayList.add(plainText);
            }

            plainText = "";
        }
        return possiblePlainTextsArrayList;

    }

    private static String getPlainDigramm(String digramm) {
        for (int i = 0; i < 26;i++) {
            for (int j = 0; j < 26; j++) {
                if (digramm.equals(keyTable[i][j])) {
                    return abc[i] + abc[j];
                }
            }
        }
        System.out.println("Das sollte nicht passieren! Stichwort: aa");
        return "ShitHappens";
    }

    public static int getLetterNumber(char letter) {
        switch (letter) {
            case 'a': return 0;
            case 'b': return 1;
            case 'c': return 2;
            case 'd': return 3;
            case 'e': return 4;
            case 'f': return 5;
            case 'g': return 6;
            case 'h': return 7;
            case 'i': return 8;
            case 'j': return 9;
            case 'k': return 10;
            case 'l': return 11;
            case 'm': return 12;
            case 'n': return 13;
            case 'o': return 14;
            case 'p': return 15;
            case 'q': return 16;
            case 'r': return 17;
            case 's': return 18;
            case 't': return 19;
            case 'u': return 20;
            case 'v': return 21;
            case 'w': return 22;
            case 'x': return 23;
            case 'y': return 24;
            case 'z': return 25;
            default: break;
        }

        System.out.println("Hier sollte niemand sein! Stichwort: 26");
        return 26;
    }

    public static ArrayList<Integer> searchPositions(String possiblePlainText, String givenWord) {
        ArrayList<Integer> integerArrayList = new ArrayList<Integer>();

        int intIndexInString = 0;

        for (int i = 0;i < possiblePlainText.length();i++) {
            intIndexInString = possiblePlainText.indexOf(givenWord, i);
            if (intIndexInString != -1) {
                integerArrayList.add(intIndexInString);
                i += intIndexInString;
            }
        }

        return integerArrayList;
    }

    public static String printBeautiful(String s) {
        StringBuilder sb = new StringBuilder();
        for(int i = 0;i < s.length();i++) {
            sb.append(s.charAt(i));
            if ((i+1)%5==0) {
                sb.append(" ");
            }
            if ((i+1)%50==0) {
                sb.append("\n");
            }
        }

        return sb.toString();
    }
}
