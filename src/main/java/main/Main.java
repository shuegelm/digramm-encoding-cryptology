package main;

import javafx.util.Pair;

import java.util.ArrayList;

/**
 * Created by liqSTAR on 26.12.2017.
 */
public class Main {

    private static final String PATH_KEY_FILENAME ="src/main/resources/keys.txt";
    private static final String PATH_PLAINTEXT_FILENAME ="src/main/resources/plaintext.txt";

    public static void main(final String[] args) {
        String[][] keyTable = new String[26][26];
        String plainTextString;
        String cipherTextString;
        String plainTextFromCipherString;
        String givenWord = "kleinbuchstaben";
        ArrayList<String> possiblePlainTextsArrayList = new ArrayList<String>();

        ArrayList<Pair<String,Double>> unsortedPairArrayList = new ArrayList<Pair<String, Double>>();
        ArrayList<Pair<String,Double>> sortedPairArrayList = new ArrayList<Pair<String, Double>>();

        //Einlesen der Schlüssel-TXT-Datei
        System.out.println("\nEinlesen der Schlüsseltabelle!");
        keyTable = FileLoader.keyTextFileLoader(PATH_KEY_FILENAME);
        System.out.println("fertig");

        //Einlesen der plain-TXT-Datei
        System.out.println("\nPlaintext wird eingelesen!");
        plainTextString = FileLoader.plainTextFileLoaderAsString(PATH_PLAINTEXT_FILENAME);
        Coding.printBeautiful(plainTextString);

        //Verschlüsseln des Textes
        System.out.println("\nPlaintext wird verschlüsselt");
        cipherTextString = Coding.encoding(plainTextString,keyTable);
        System.out.println(Coding.printBeautiful(cipherTextString));

        //CipherText mit Schlüssel entschlüsseln
        System.out.println("\nCiphertext wird mit Schlüssel entschlüsselt");
        plainTextFromCipherString = Coding.decodingWithKey(cipherTextString,keyTable);
        System.out.println(Coding.printBeautiful(plainTextFromCipherString));

        if (plainTextString.equals(plainTextFromCipherString)) {
            System.out.println("Verschlüsseln und Entschlüsseln mit dem selben Schlüssel hat geklappt");
        }
        else {
            System.out.println("Falsch encodiert und decodiert");
        }


        //Angriff auf Ciphertext
        System.out.println("\nStarting Attack on cipher Text");
        unsortedPairArrayList = Coding.countDigramms(cipherTextString);
        sortedPairArrayList = Quicksort.quickSort(unsortedPairArrayList,0,unsortedPairArrayList.size()-1);

        System.out.println("Die Häufigkeiten:");
        for (Pair<String,Double> pair : sortedPairArrayList) {
            System.out.println("Das Digramm " + pair.getKey() + " hat die Häufigkeit: " + pair.getValue());
        }

        possiblePlainTextsArrayList = Coding.decideAndReplace(sortedPairArrayList, cipherTextString, givenWord);
        for (String s : possiblePlainTextsArrayList) {
            System.out.println("\nDas kam nach dem Angriff raus: \n"+s);
            System.out.println("\nSo sollte es sein:");
            System.out.println(Coding.printBeautiful(plainTextString));
        }

    }
}